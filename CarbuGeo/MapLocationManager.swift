/*
 
 */

import Foundation
import SwiftUI
import MapKit

final class MapLocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {

    /*
        Declaration of region for map display, to be changed with user location to center
        map on user current location
     */
    @Published var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 37.331516, longitude: -121.89), span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))

    var locationManager: CLLocationManager?

    /*
        Check if location service is enable on device
     */
    func checkEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager!.delegate = self
        } else {
            print("Not enabled")
        }
    }

    /*
        Check if Location Request is accepted by the user
     */
    func checkAuth() {
        guard let locationManager = locationManager else {
            return
        }

        switch locationManager.authorizationStatus {
            case .notDetermined: locationManager.requestWhenInUseAuthorization()
            case .restricted: print("restricted")
            case .denied: print("denied")
            case .authorizedAlways, .authorizedWhenInUse:
                // If location authorized, set region to user's location
                region = MKCoordinateRegion(center: locationManager.location!.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
                break
            @unknown default: print("unknown")
        }
    }

    /*
        Check location status on authorization status change
     */
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkAuth()
    }
}
