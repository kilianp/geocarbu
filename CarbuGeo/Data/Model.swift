// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation
import MapKit

// MARK: - Welcome
struct Welcome: Codable {
    let nhits: Int
    let parameters: Parameters
    let records: [Record]
    let facetGroups: [FacetGroup]?

    enum CodingKeys: String, CodingKey {
        case nhits, parameters, records
        case facetGroups = "facet_groups"
    }
}

// MARK: - FacetGroup
struct FacetGroup: Codable {
    let name: String
    let facets: [Facet]
}

// MARK: - Facet
struct Facet: Codable {
    let name: String
    let count: Int
    let state: State
    let path: String
}

enum State: String, Codable {
    case displayed = "displayed"
}

// MARK: - Parameters
struct Parameters: Codable {
    let dataset: Dataset
    let rows, start: Int
    let facet: [String]
    let format, timezone: String
}

enum Dataset: String, Codable {
    case prixDESCarburantsJ7 = "prix_des_carburants_j_7"
}

// MARK: - Record
struct Record: Codable {
    let datasetid: Dataset
    let recordid: String
    let fields: Fields
    let geometry: Geometry?
    let recordTimestamp: String

    enum CodingKeys: String, CodingKey {
        case datasetid, recordid, fields, geometry
        case recordTimestamp = "record_timestamp"
    }
}

// MARK: - Fields
struct Fields: Codable {
    let geoPoint: [Double]?
    let city: String
    let automate24_24, timetable, services, name: String?
    let brand: String?
    let pop: Pop
    let shortage: String?
    let cp, id, fuel, address: String
    let update: String
    let priceGazole: Double?
    let priceSp95, priceE10, priceGplc, priceSp98: Double?
    let priceE85: Double?

    enum CodingKeys: String, CodingKey {
        case geoPoint = "geo_point"
        case city
        case automate24_24 = "automate_24_24"
        case timetable, services, name, brand, pop, shortage, cp, id, fuel, address, update
        case priceGazole = "price_gazole"
        case priceSp95 = "price_sp95"
        case priceE10 = "price_e10"
        case priceGplc = "price_gplc"
        case priceSp98 = "price_sp98"
        case priceE85 = "price_e85"
    }
}

enum Pop: String, Codable {
    case a = "A"
    case r = "R"
}

// MARK: - Geometry
struct Geometry: Codable {
    let type: String
    let coordinates: [Double]
}





/*
 
 
                Models for stations annotations
    
 
 */



struct StationsInfos{
    let prices: MinPrices?
    let markers : [StationMarker]?
}

struct MinPrices: Codable{
    let gazole: Double?
    let sp95: Double?
    let sp98: Double?
    let e10: Double?
    let e85: Double?
    let gpl: Double?
}

class StationMarker: NSObject, MKAnnotation,Identifiable {
    let id = UUID()
    let title: String?
    let subtitle: String?
    let field: Fields
    let coordinate: CLLocationCoordinate2D
    let cheapest: Bool?
    
    init(
        title: String?,
         subtitle: String?,
         field: Fields,
         coordinate: CLLocationCoordinate2D,
         cheapest: Bool) {
             
        self.title = title
        self.field = field
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.cheapest = cheapest
    }
}
