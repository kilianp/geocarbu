//
//  Data.swift
//  geocarbu
//
//  Created by Kilian Pichard on 17/05/2022.


import Foundation
import MapKit

class Api {
    static func fetchStations(position: MKCoordinateRegion, distance:Int) async throws -> ([Record]) {
        //  url for the api call, var for user position and max diameter distance
        let url = URL(string: "https://public.opendatasoft.com/api/records/1.0/search/?dataset=prix_des_carburants_j_7&q=&lang=fr&rows=1000&facet=cp&facet=pop&facet=city&facet=automate_24_24&facet=fuel&facet=shortage&facet=update&facet=services&facet=brand&geofilter.distance=\(position.center.latitude)%2C\(position.center.longitude)%2C\(distance)")!

        
        // Use the async variant of URLSession to fetch data
        let (data, _) = try await URLSession.shared.data(from: url)
        
        // Parse the JSON data
        let result = try JSONDecoder().decode(Welcome.self, from: data)
        
        //  Return the stations
        return result.records
    }
}
