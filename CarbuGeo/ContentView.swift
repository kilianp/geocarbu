import MapKit
import SwiftUI


struct ContentView: View {

    // Create a location manager to retrieve user's location and displayed it on the map
    @StateObject private var mapLocation = MapLocationManager()

    //  Current selected fuel & distance option
    @SwiftUI.State private var selectedFuel = "Tout Voir"
    @SwiftUI.State private var selectedDistance = 5
    
    @SwiftUI.State private var stationsLoaded = false

    //  All stations informations for selected fuel and distance
    @SwiftUI.State var stations: StationsInfos = StationsInfos(prices: MinPrices(gazole: 0.00, sp95: 0.00, sp98: 0.00, e10: 0.00, e85: 0.00, gpl: 0.00), markers: [])

    // Proposition of selection for fuel picker
    var fuels = ["Tout Voir", "Gazole", "SP98", "SP95", "E10", "E85", "GPL"]
    // Proposition of selection for distance picker
    var distances = [1, 2, 5, 10, 15, 20, 50, 100]

    var body: some View {
        NavigationView {
            VStack {
                ZStack {
                    /*
                        Display of the map center on user's location
                        and display all stations with navigation links to go to station details view
                        and image map pin
                     */
                    Map(coordinateRegion: $mapLocation.region, showsUserLocation: true, annotationItems: stations.markers!) { location in
                        MapAnnotation(
                            coordinate: location.coordinate
                        ) {
                            NavigationLink(destination: { StationView(station: location, prices: stations.prices) }, label: { VStack {
                                Image(location.cheapest ?? false ? "map-pin-cheap" : "map-pin")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(height: 35)
                                    .font(.title)
                                    .foregroundColor(location.cheapest ?? false ? .green : .red)

                            } })

                        }
                    }.ignoresSafeArea()
                    /*
                        If there is no station (api call not finished), recreation of the launch screen
                        until there is data to display
                    */
                    if(!stationsLoaded) {
                        ZStack(alignment: .center) {
                            Rectangle()
                                .fill(Color("backgroundApp"))
                                .frame(width: 2000, height: 2000, alignment: .center)
                            Image("map-pin").resizable().aspectRatio(contentMode: .fit).frame(height: 68).offset(y: -32)
                        }


                    }
                }

                //  Bottom pickers
                HStack {
                    VStack{
                        Text("Carburant : ")
                        Picker(selection: $selectedFuel, label: Text("Color")) {
                            ForEach(fuels, id: \.self) {
                                Text("\($0)")
                            }
                        }
                        .onChange(of: selectedFuel) {
                            _ in
                            // On fuel picker change, renew the api call to get new stations
                            Task {
                                stations = await fetchData(mapLocation.region,selectedFuel, selectedDistance * 1000)
                            }
                        }
                        
                    }
                    Spacer()
                    VStack{
                        Text("Distance : ")
                        Picker(selection: $selectedDistance, label: Text("Color")) {
                            ForEach(distances, id: \.self) {
                                Text("\($0) Km")
                            }
                        }
                        .onChange(of: selectedDistance) {
                            // On distance picker change, renew the api call to get new stations
                            _ in
                            Task {
                                stations = await fetchData(mapLocation.region,selectedFuel, selectedDistance * 1000)
                            }
                        }
                    }
                }.padding([.leading, .trailing], 20)

                Spacer()

            }
        }
            .ignoresSafeArea().onAppear(perform: {
                // On view appear, check location and retrieve stations data for all fuel and 5Km diameter
                mapLocation.checkEnabled()
                Task {
                    stations = await fetchData(mapLocation.region,selectedFuel, selectedDistance * 1000)
                    stationsLoaded = true
                }
        })
    }


}

/*
    Function to call mocked data for stations
 */
func fetchData(_ region: MKCoordinateRegion,_ fuel: String, _ distance: Int) async -> StationsInfos {
    return await StationAnnotation().requestMockData(region: region, distance: distance, fuel: fuel)
}

struct ContentView_Preview: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
