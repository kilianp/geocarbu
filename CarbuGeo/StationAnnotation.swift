//
//  LandmarkAnnotation.swift
//  SwiftUI-MapView
//
//  Created by Anand Nimje on 12/12/19.
//  Copyright © 2019 Anand. All rights reserved.
//

import MapKit
import SwiftUI

class StationAnnotation {

    
    func requestMockData(region: MKCoordinateRegion, distance: Int, fuel: String) async -> StationsInfos {
        
        var stations: [StationMarker] = []
        var priceMin: MinPrices?
        
        do {
            stations = []
            var stationsFiltered: [Record] = []
            // fetch station from a region and a radius
            let stationsFetched = try await Api.fetchStations(position: region, distance: distance)

            
            switch fuel {
            case "Gazole":
                stationsFiltered = stationsFetched.filter {
                    fuel in
                    fuel.fields.priceGazole != nil
                }.sorted(by: { $0.fields.priceGazole! < $1.fields.priceGazole! })
            case "SP98":
                stationsFiltered = stationsFetched.filter {
                    fuel in
                    fuel.fields.priceSp98 != nil
                }.sorted(by: { $0.fields.priceSp98! < $1.fields.priceSp98! })
            case "SP95":
                stationsFiltered = stationsFetched.filter {
                    fuel in
                    fuel.fields.priceSp95 != nil
                }.sorted(by: { $0.fields.priceSp95! < $1.fields.priceSp95! })
            case "SP95-E10":
                stationsFiltered = stationsFetched.filter {
                    fuel in
                    fuel.fields.priceE10 != nil
                }.sorted(by: { $0.fields.priceE10! < $1.fields.priceE10! })
            case "E85":
                stationsFiltered = stationsFetched.filter {
                    fuel in
                    fuel.fields.priceE85 != nil
                }.sorted(by: { $0.fields.priceE85! < $1.fields.priceE85! })
            case "GPL":
                stationsFiltered = stationsFetched.filter {
                    fuel in
                    fuel.fields.priceGplc != nil
                }.sorted(by: { $0.fields.priceGplc! < $1.fields.priceGplc! })
            default:
                print("ok");
                stationsFiltered = stationsFetched
            }
            
            stationsFiltered = stationsFiltered.filter({
                fuel in
                    fuel.fields.priceGazole ?? 0.00 < 1
                    &&
                    fuel.fields.priceE85 ?? 0.00 < 1
                    &&
                    fuel.fields.priceE10 ?? 0.00 < 1
                    &&
                    fuel.fields.priceGplc ?? 0.00 < 1
                    &&
                    fuel.fields.priceSp95 ?? 0.00 < 1
                    &&
                    fuel.fields.priceSp98 ?? 0.00 < 1
            })
            
            let minGazole = stationsFiltered.min(by: {$0.fields.priceGazole ?? 1000 < $1.fields.priceGazole ?? 1000})?.fields.priceGazole ?? 0.00
            let minE85 = stationsFiltered.min(by: {$0.fields.priceE85 ?? 1000 < $1.fields.priceE85 ?? 1000})?.fields.priceE85 ?? 0.00
            let minGPL = stationsFiltered.min(by: {$0.fields.priceGplc ?? 1000 < $1.fields.priceGplc ?? 1000})?.fields.priceGplc ?? 0.00
            let minSP95 = stationsFiltered.min(by: {$0.fields.priceSp95 ?? 1000 < $1.fields.priceSp95 ?? 1000})?.fields.priceSp95 ?? 0.00
            let minSP95E10 = stationsFiltered.min(by: {$0.fields.priceE10 ?? 1000 < $1.fields.priceE10 ?? 1000})?.fields.priceE10 ?? 0.00
            let minSP98 = stationsFiltered.min(by: {$0.fields.priceSp98 ?? 1000 < $1.fields.priceSp98 ?? 1000})?.fields.priceSp98 ?? 0.00
            
            priceMin = MinPrices(gazole: minGazole, sp95: minSP95, sp98: minSP98, e10: minSP95E10, e85: minE85, gpl: minGPL)
            
            for (index, station) in stationsFiltered.enumerated() {
                if station.fields.geoPoint != nil {
                    stations.append(
                        StationMarker(
                            title: station.fields.name,
                            subtitle: station.fields.address,
                            field: station.fields,
                            coordinate: .init(
                                latitude: station.fields.geoPoint![0], longitude: station.fields.geoPoint![1]),
                            cheapest: index == 0
                        )

                    )
                }
            }
        } catch {
            print("Request failed with error: \(error)")
        }
        return StationsInfos(prices: priceMin, markers: stations)
    }
}



