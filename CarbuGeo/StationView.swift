//
//  StationInfos.swift
//  CarbuGeo
//
//  Created by Kilian Pichard on 18/05/2022.
//

import SwiftUI
import Foundation
struct StationView: View {
    @SwiftUI.State var station: StationMarker?
    @SwiftUI.State var prices: MinPrices?

    func formatDate(_ date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
        return dateFormatter.date(from: date)!
    }
    func relativeDaysFromToday(_ startDate: Date, _ endDate: Date) -> Int
    {
        let diffComponents = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        let days = diffComponents.day
        return days ?? 0
    }


    var body: some View {
        VStack {
            Text("\(station!.field.name!)").font(.title)
            Text("\(station!.field.address)")
            Divider()
            HStack(alignment: .center) {
                Text("Dernière mise à jour : ").bold()
                Spacer()
                Text("\(relativeDaysFromToday(formatDate(station!.field.update), Date())) jours")
                    .padding(5)
                    .foregroundColor(.white)
                    .background((relativeDaysFromToday(formatDate(station!.field.update), Date()) <= 1 ? .green : relativeDaysFromToday(formatDate(station!.field.update), Date()) <= 4 ? .orange : .red))
                    .cornerRadius(5)

            }.padding([.leading, .trailing], 20)
            List() {
                if(station!.field.priceGazole != nil) {
                    HStack(alignment: .center) {
                        Text("Gazole").font(.title)
                        Spacer()
                        Text("\(String(format: "%.3f", station!.field.priceGazole! * 1000)) €")
                            .padding(5)
                            .background(station!.field.priceGazole == (prices?.gazole) ? .green:.white)
                            .foregroundColor(station!.field.priceGazole == (prices?.gazole) ? .white:.black)
                            .cornerRadius(5)
                    }
                }
                if(station!.field.priceSp98 != nil) {
                    HStack(alignment: .center) {
                        Text("SP98").font(.title)
                        Spacer()
                        Text("\(String(format: "%.3f", station!.field.priceSp98! * 1000)) €")
                            .padding(5)
                            .background(station!.field.priceSp98 == (prices?.sp98) ? .green:.white)
                            .foregroundColor(station!.field.priceSp98 == (prices?.sp98) ? .white:.black)
                            .cornerRadius(5)
                    }
                }
                if(station!.field.priceSp95 != nil) {
                    HStack(alignment: .center) {
                        Text("SP95").font(.title)
                        Spacer()
                        Text("\(String(format: "%.3f", station!.field.priceSp95! * 1000)) €")
                            .padding(5)
                            .background(station!.field.priceSp95 == (prices?.sp95) ? .green:.white)
                            .foregroundColor(station!.field.priceSp95 == (prices?.sp95) ? .white:.black)
                            .cornerRadius(5)
                    }
                }
                if(station!.field.priceE10 != nil) {
                    HStack(alignment: .center) {
                        Text("SP95-E10").font(.title)
                        Spacer()
                        Text("\(String(format: "%.3f", station!.field.priceE10! * 1000)) €")
                            .padding(5)
                            .background(station!.field.priceE10 == (prices?.e10) ? .green:.white)
                            .foregroundColor(station!.field.priceE10 == (prices?.e10) ? .white:.black)
                            .cornerRadius(5)
                    }
                }
                if(station!.field.priceE85 != nil) {
                    HStack(alignment: .center) {
                        Text("E85").font(.title)
                        Spacer()
                        Text("\(String(format: "%.3f", station!.field.priceE85! * 1000)) €")
                            .padding(5)
                            .background(station!.field.priceE85 == (prices?.e85) ? .green:.white)
                            .foregroundColor(station!.field.priceE85 == (prices?.e85) ? .white:.black)
                            .cornerRadius(5)
                    }
                }
                if(station!.field.priceGplc != nil) {
                    HStack(alignment: .center) {
                        Text("GPL").font(.title)
                        Spacer()
                        Text("\(String(format: "%.3f", station!.field.priceGplc! * 1000)) €")
                            .padding(5)
                            .background(station!.field.priceGplc == (prices?.gpl) ? .green:.white)
                            .foregroundColor(station!.field.priceGplc == (prices?.gpl) ? .white:.black)
                            .cornerRadius(5)
                    }
                }
            }

        }
    }
}


struct StationView_Previews: PreviewProvider {
    static var previews: some View {
        StationView()
    }
}
