//
//  CarbuGeoApp.swift
//  CarbuGeo
//
//  Created by Kilian Pichard on 18/05/2022.
//

import SwiftUI

@main
struct CarbuGeoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
